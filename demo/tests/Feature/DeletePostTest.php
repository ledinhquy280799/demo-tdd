<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeletePostTest extends TestCase
{
    /** @test */
    public function user_can_delete_post_if_post_exist_and_user_login()
    {
        $this->withoutExceptionHandling();
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->create();
        $countNumberPostBeforeDelete = Post::count();
        $response = $this->delete($this->getDeletePostRoute($post->id));
        $this->assertDatabaseMissing('posts', [
            'id' => $post->id,
            'title' => $post->title,
            'content' => $post->content,
        ]);
        $countNumberPostAfterDelete = Post::count();
        $response->assertRedirect(route('posts.index'));
        $response->assertStatus(302);
        $this->assertEquals($countNumberPostBeforeDelete - 1, $countNumberPostAfterDelete);
    }

    /** @test */
    public function user_can_not_delete_post_if_post_not_exist_and_user_login()
    {
        $this->actingAs(User::factory()->create());
        $postId = -1;
        $response = $this->delete($this->getDeletePostRoute($postId));
        $response->assertStatus(404);
    }

     /** @test */
     public function user_can_not_delete_post_if_post_exist_and_user_not_login()
     {
         $post = Post::factory()->create();
         $response = $this->delete($this->getDeletePostRoute($post->id));
         $response->assertRedirect('/login');
         $response->assertStatus(302);
     }

    public function getDeletePostRoute(int $id)
    {
        return route('posts.destroy', $id);
    }
}
