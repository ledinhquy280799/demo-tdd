<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreatePostTest extends TestCase
{
    /** @test */
    public function user_can_create_post_if_user_login()
    {
        $countNumberPostBeforeCreate = Post::count();
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->make()->toArray();
        $response = $this->post($this->getCreatePostRoute(), $post);
        $this->assertDatabaseHas('posts', $post);
        $countNumberPostAfterCreate = Post::count();
        $this->assertEquals($countNumberPostBeforeCreate + 1, $countNumberPostAfterCreate);
        $response->assertRedirect(route('posts.index'));
        $response->assertStatus(302);
    }

    /** @test */
    public function user_can_not_create_post_if_user_login_but_title_field_is_null()
    {
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->make(['title' => null])->toArray();
        $response = $this->post($this->getCreatePostRoute(), $post);
        $response->assertSessionHasErrors(['title']);
    }

    /** @test */
    public function user_can_not_create_post_if_user_not_login()
    {
        $post = Post::factory()->make()->toArray();
        $response = $this->post($this->getCreatePostRoute(), $post);
        $response->assertRedirect('/login');
        $response->assertStatus(302);
    }

    public function getCreatePostRoute()
    {
        return route('posts.create');
    }
}
