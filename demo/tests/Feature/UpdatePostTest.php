<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdatePostTest extends TestCase
{
    /** @test */
    public function user_can_update_post_if_post_exist_and_user_login()
    {
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->create();
        $dataUpdate = [
            'title' => $this->faker->name,
            'content' => $this->faker->text,
        ];
        $response = $this->put($this->getUpdatePostRoute($post->id), $dataUpdate);
        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'title' => $dataUpdate['title'],
            'content' => $dataUpdate['content']
        ]);
        $response->assertRedirect(route('posts.index'));
        $response->assertStatus(302);
    }

    /** @test */
    public function user_can_not_update_post_if_post_not_exist_and_user_login()
    {
        $this->actingAs(User::factory()->create());
        $postId = -1;
        $dataUpdate = [
            'title' => $this->faker->name,
            'content' => $this->faker->text,
        ];
        $response = $this->put($this->getUpdatePostRoute($postId), $dataUpdate);
        $response->assertStatus(404);
    }

    /** @test */
    public function user_can_not_update_post_if_post_exist_and_user_not_login()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'title' => $this->faker->name,
            'content' => $this->faker->text,
        ];
        $response = $this->put($this->getUpdatePostRoute($post->id), $dataUpdate);
        $response->assertRedirect('/login');
        $response->assertStatus(302);
    }

    /** @test */
    public function user_can_not_update_post_if_post_exist_and_user_login_but_title_field_is_null()
    {
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->create();
        $dataUpdate = [
            'title' => '',
            'content' => $this->faker->text,
        ];
        $response = $this->put($this->getUpdatePostRoute($post->id), $dataUpdate);
        $response->assertSessionHasErrors(['title']);
    }

    public function getUpdatePostRoute(int $id)
    {
        return route('posts.update', $id);
    }
}
