<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetListPostTest extends TestCase
{
    /** @test */
    public function user_can_get_list_post_if_user_login()
    {
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->create();
        $response = $this->get($this->getGetListPostRoute());

        $response->assertStatus(200);
        $response->assertViewIs('posts.index');
        $response->assertSee($post->name);
    }

    /** @test */
    public function user_can_get_list_post_if_user_not_login()
    {
        $post = Post::factory()->create();
        $response = $this->get($this->getGetListPostRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(302);
    }

    public function getGetListPostRoute()
    {
        return route('posts.index');
    }
}
