<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostController extends Controller
{

    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function index()
    {
        return view('posts.index');
    }

    public function show(int $id)
    {
        $post = $this->post->findOrFail($id);
        return view('posts.show', compact('post'));
    }

    public function store(StorePostRequest $request)
    {
        $this->post->create($request->all());
        return redirect()->route('posts.index');
    }

    public function update(UpdatePostRequest $request, int $id)
    {
        $post = $this->post->findOrFail($id);
        $post->update($request->all());
        return redirect()->route('posts.index');
    }

    public function destroy(int $id)
    {
        $post = $this->post->findOrFail($id);
        $post->delete();
        return redirect()->route('posts.index');
    }
}
